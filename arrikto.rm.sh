#!/usr/bin/env bash

kfapp=$1
kfdir=$2

## DIR where the current script resides
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
#
#kubectl delete -f "${DIR}/ingress.yml"

export KF_DIR=${kfdir}/${kfapp}
#export CONFIG_FILE=${KF_DIR}/kfctl_existing_arrikto.0.7.1.yaml
#export CONFIG_FILE=${KF_DIR}/kfctl_istio_dex.v1.1.0.yaml
export CONFIG_FILE=${KF_DIR}/kfctl_istio_dex.v1.2.0.yaml

# If you want to delete all the resources, run:
cd "${KF_DIR}" && kfctl delete -f "${CONFIG_FILE}" --delete_storage --force-deletion
