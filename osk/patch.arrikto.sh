#!/usr/bin/env bash

kubectl patch service -n istio-system istio-ingressgateway -p '{"spec": {"type": "NodePort"}}'
