#!/usr/bin/env bash

kubectl patch service -n kubernetes-dashboard kubernetes-dashboard -p '{"spec": {"type": "NodePort"}}'
