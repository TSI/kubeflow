#!/usr/bin/env bash

function delete() {
  local resource=$1 && shift
  local namespaces=("$@")

  for namespace in "${namespaces[@]}"; do
    echo "=== Deleting ${resource} in ${namespace} ==="
    kubectl get "${resource}" -n "${namespace}" | grep -v 'NAME' | cut -d ' ' -f 1 | xargs kubectl delete "${resource}" -n "${namespace}"
  done

  echo "kubectl get ${resource} -A"
  kubectl get "${resource}" -A
}

# ML and non-ML pipelines
namespaces=("davidyuan") && delete sts "${namespaces[@]}"

# stateful set
namespaces=("kubeflow" "istio-system") && delete sts "${namespaces[@]}"

# deploy
namespaces=("kubeflow" "istio-system" "knative-serving" "auth" "cert-manager") && delete deploy "${namespaces[@]}"

# job.batch
namespaces=("istio-system" "kubeflow") && delete job.batch "${namespaces[@]}"

# daemonset.apps
namespaces=("istio-system") && delete daemonset.apps "${namespaces[@]}"

# horizontalpodautoscaler.autoscaling
namespaces=("istio-system" "knative-serving") && delete horizontalpodautoscaler.autoscaling "${namespaces[@]}"

# svc
namespaces=("kubeflow" "istio-system" "davidyuan" "knative-serving" "auth" "cert-manager") && delete svc "${namespaces[@]}"

# configmap
kubectl get configmap -n kube-system --show-kind | grep cert-manager | cut -d ' ' -f 1 | xargs kubectl delete -n kube-system
namespaces=("auth" "cert-manager" "kubeflow" "istio-system" "knative-serving") && delete configmap "${namespaces[@]}"

# role and rolebinding
kubectl get role -n kube-system --show-kind | grep cert-manager | cut -d ' ' -f 1 | xargs kubectl delete -n kube-system
kubectl get rolebinding -n kube-system --show-kind | grep cert-manager | cut -d ' ' -f 1 | xargs kubectl delete -n kube-system
namespaces=("istio-system" "kubeflow") && delete role "${namespaces[@]}"
namespaces=("istio-system" "kubeflow" "knative-serving" "davidyuan") && delete rolebinding "${namespaces[@]}"

# pvc
namespaces=("knative-serving" "istio-system" "kubeflow") && delete po "${namespaces[@]}"
namespaces=("davidyuan" "kubeflow" "istio-system") && delete pvc "${namespaces[@]}"

# clusterrole and clusterrrolebinding
function delete-cluster() {
    local resource=$1 && shift
    local keys=("$@")

    for key in "${keys[@]}"; do
      echo "=== Deleting ${resource} named as ${key} ==="
      kubectl get "${resource}" --show-kind | grep "${key}" | cut -d ' ' -f 1 | xargs kubectl delete
    done

    echo "kubectl get ${resource}"
    kubectl get "${resource}"
}
keywords=("kubeflow" "istio" "cert-manager" "knative-serving" "argo" "ml-pipeline" "katib" "dex" "jupyter-web-app" "tf-job-operator" "admission-webhook-cluster-role" "application-controller-cluster-role" "centraldashboard" "custom-metrics-server-resources" "kfserving-proxy-role" "manager-role" "notebook-controller-role" "pipeline-runner" "pytorch-operator" "spark-operatoroperator-cr" "spartakus") && delete-cluster clusterrole "${keywords[@]}"
keywords=("istio" "cert-manager" "dex" "argo" "jupyter-web-app" "katib" "seldon-manager" "ml-pipeline" "admission-webhook-cluster-role-binding" "application-controller-cluster-role-binding" "centraldashboard" "custom-metrics:system:auth-delegator" "hpa-controller-custom-metrics" "kfserving-proxy-rolebinding" "knative-serving-controller-admin" "manager-rolebinding" "notebook-controller-role-binding" "pipeline-runner" "profiles-cluster-role-binding" "pytorch-operator" "spark-operatorsparkoperator-crb" "spartakus" "tf-job-operator") && delete-cluster clusterrolebinding "${keywords[@]}"
keywords=("cert-manager-webhook" "config.webhook.serving.knative.dev" "inferenceservice.serving.kubeflow.org" "istio-galley" "katib-validating-webhook-config" "seldon-validating-webhook-configuration-kubeflow" "validation.webhook.serving.knative.dev") && delete-cluster validatingwebhookconfiguration "${keywords[@]}"
keywords=("admission-webhook-mutating-webhook-configuration" "cert-manager-webhook" "inferenceservice.serving.kubeflow.org" "istio-sidecar-injector" "katib-mutating-webhook-config" "seldon-mutating-webhook-configuration-kubeflow" "webhook.serving.knative.dev") && delete-cluster mutatingwebhookconfiguration "${keywords[@]}"

# clusterrole and clusterrolebinding are not namespaced
#kubectl get apiservices v1beta1.webhook.cert-manager.io
kubectl delete apiservices v1beta1.webhook.cert-manager.io

#custom.metrics.k8s.io/v1beta1
#kubectl get apiservices v1beta1.custom.metrics.k8s.io
kubectl delete apiservices v1beta1.custom.metrics.k8s.io

namespaces=("istio-system") && delete poddisruptionbudget "${namespaces[@]}"

# Output format is inconsistent, thus difficult to automate
function delete-api-resources() {
    local namespaces=("$@")

    for namespace in "${namespaces[@]}"; do
      echo "=== Deleting api-resources in ${namespace} ==="
      kubectl api-resources --verbs=list --namespaced -o name |
        xargs -n 1 kubectl get --show-kind --ignore-not-found -n "${namespace}" | grep -v NAME | grep -v OBJECT | grep -v poddisruptionbudget | grep -v Warning | grep -v Normal | cut -d ' ' -f 1 |
        xargs kubectl delete -n "${namespace}"
    done
    kubectl get apiservice | grep False
}
namespaces=("auth" "cert-manager" "kubeflow" "knative-serving" "istio-system") && delete-api-resources "${namespaces[@]}"
#namespaces=("kubernetes-dashboard") && delete-api-resources "${namespaces[@]}"

function get-api-resources() {
    local namespaces=("$@")

    for namespace in "${namespaces[@]}"; do
      echo "=== Checking api-resources in ${namespace} ==="
      kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind -n "${namespace}"
#      kubectl get "$(kubectl api-resources --verbs=list -o name | paste -sd, -)" --ignore-not-found --show-kind -n "${namespace}"
    done
    kubectl get apiservice | grep False
}
namespaces=("auth" "cert-manager" "kubeflow" "knative-serving" "istio-system") && get-api-resources "${namespaces[@]}"
#namespaces=("kubernetes-dashboard") && get-api-resources "${namespaces[@]}"

#kubectl get "$(kubectl api-resources --verbs=list -o name | paste -sd, -)" --ignore-not-found --all-namespaces

#kubectl get po -n knative-serving | grep -v NAME | cut -d ' ' -f 1 | xargs kubectl delete po -n knative-serving
#kubectl get po -n istio-system | grep ContainerCreating | cut -d ' ' -f 1 | xargs kubectl delete po -n istio-system

#rm -R /Users/davidyuan/IdeaProjects/davidyuan/kubeflow/osk/tsi-osk-1

kubectl delete clusterissuer.cert-manager.io/kubeflow-self-signing-issuer
#kubectl get crds | grep -E 'kubeflow|dex'
kubectl get crds | grep -v '2020-02-' | grep -v 'NAME' | cut -d ' ' -f 1 | xargs kubectl delete crds

# serviceaccount
namespaces=("auth" "cert-manager" "kubeflow" "istio-system" "knative-serving") && delete serviceaccount "${namespaces[@]}"

# secret
namespaces=("auth" "cert-manager" "kubeflow" "istio-system" "knative-serving") && delete secret "${namespaces[@]}"

kubectl delete ns davidyuan auth knative-serving cert-manager istio-system kubeflow

#kubectl api-resources --verbs=list -o name | grep -v '^componentstatuses$'| xargs -n 1 kubectl get --all-namespaces --show-kind --ignore-not-found -l kustomize.component
#
#kubectl api-resources --verbs=list -o name | grep -v '^componentstatuses$'| xargs -n 1 kubectl get --all-namespaces --show-kind --ignore-not-found -l app.kubernetes.io/part-of=kubeflow





