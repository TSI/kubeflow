#!/usr/bin/env bash

kubectl delete -f https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/raw/external/tsi-cc/ResOps/scripts/k8s-dashboard/admin-user.yml

#kubectl delete -f https://github.com/kubernetes/dashboard/raw/master/aio/deploy/recommended.yaml
kubectl delete -n kubernetes-dashboard deployment.apps/dashboard-metrics-scraper
kubectl delete -n kubernetes-dashboard deployment.apps/kubernetes-dashboard
kubectl delete -n kubernetes-dashboard serviceaccount/kubernetes-dashboard
kubectl delete -n kubernetes-dashboard service/kubernetes-dashboard
kubectl delete -n kubernetes-dashboard secret/kubernetes-dashboard-certs
kubectl delete -n kubernetes-dashboard secret/kubernetes-dashboard-csrf
kubectl delete -n kubernetes-dashboard secret/kubernetes-dashboard-key-holder
kubectl delete -n kubernetes-dashboard configmap/kubernetes-dashboard-settings
kubectl delete -n kubernetes-dashboard role.rbac.authorization.k8s.io/kubernetes-dashboard
kubectl delete clusterrole.rbac.authorization.k8s.io/kubernetes-dashboard
kubectl delete -n kubernetes-dashboard rolebinding.rbac.authorization.k8s.io/kubernetes-dashboard
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/kubernetes-dashboard
kubectl delete -n kubernetes-dashboard service/dashboard-metrics-scraper
kubectl delete namespace/kubernetes-dashboard
