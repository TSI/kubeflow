#!/usr/bin/env bash

# https://www.kubeflow.org/docs/gke/deploy/oauth-setup/

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Set the Kubernetes version as found in the UCP Dashboard or API
#https://github.com/kubeflow/kfctl/releases/download/v1.1.0/kfctl_v1.1.0-0-g9a3621e_linux.tar.gz
#https://github.com/kubeflow/kfctl/releases/download/v1.2.0/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz
kfctlversion='v1.2.0' # 'v1.1.0'
tag='-0-gbc038f9' # '-0-g9a3621e'

#osname=darwin or linux
osname=$(uname | tr '[:upper:]' '[:lower:]')

cd "${DIR}" && curl -LO "https://github.com/kubeflow/kfctl/releases/download/${kfctlversion}/kfctl_${kfctlversion}${tag}_${osname}.tar.gz" && \
  tar -xvf "kfctl_${kfctlversion}${tag}_${osname}.tar.gz"

# Make the kfctl binary executable.
chmod +x "${DIR}/kfctl"
"${DIR}/kfctl" version

rm "${DIR}/kfctl_${kfctlversion}${tag}_${osname}.tar.gz"

# Move the kfctl executable to /usr/local/bin.
# sudo mv "${DIR}/kfctl" /usr/local/bin/kfctl
