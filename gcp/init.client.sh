#!/usr/bin/env bash

PROJECT=$1
CLUSTER=$2
ZONE=$3

gcloud auth activate-service-account --key-file="${GOOGLE_APPLICATION_CREDENTIALS}"

# CLUSTER="tsi-gcp-1"; ZONE="europe-west4-a"; PROJECT="dy-test-266411"; REGION="europe-west4"
# CLUSTER="dlf-gcp-1"; ZONE="europe-west4-a"; PROJECT="dataset-lifecycle-284718"; REGION="europe-west4"
gcloud container clusters get-credentials "${CLUSTER}" --zone "${ZONE}" --project "${PROJECT}"
gcloud config list
