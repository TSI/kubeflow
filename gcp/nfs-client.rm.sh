#!/usr/bin/env bash

# https://helm.sh/docs/intro/install/
# https://github.com/helm/charts/tree/master/stable/nfs-client-provisioner

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm uninstall nfs
kubectl get storageclass
