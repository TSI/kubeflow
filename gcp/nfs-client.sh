#!/usr/bin/env bash

# https://helm.sh/docs/intro/install/
# https://github.com/helm/charts/tree/master/stable/nfs-client-provisioner

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm install nfs stable/nfs-client-provisioner --set=storageClass.name=nfs-client --set nfs.server=10.232.71.226 --set nfs.path=/export
kubectl rollout status deployment.v1.apps/nfs-nfs-client-provisioner --timeout=6m

kubectl get storageclass