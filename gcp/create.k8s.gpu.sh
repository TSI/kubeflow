#!/usr/bin/env bash

# https://cloud.google.com/kubernetes-engine/docs/how-to/gpus

PROJECT=$1
CLUSTER=$2
ZONE=$3
REGION=$4

gcloud auth activate-service-account --key-file="${GOOGLE_APPLICATION_CREDENTIALS}"
#e2-standard-4
# e2-highcpu-32
gcloud container --project "${PROJECT}" clusters create "${CLUSTER}" --zone "${ZONE}" --no-enable-basic-auth \
  --machine-type "e2-highcpu-16" --image-type "COS" --disk-type "pd-standard" \
  --disk-size "100" --metadata disable-legacy-endpoints=true \
  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
  --num-nodes "1" --enable-ip-alias --enable-stackdriver-kubernetes \
  --network "projects/${PROJECT}/global/networks/default" \
  --subnetwork "projects/${PROJECT}/regions/${REGION}/subnetworks/default" --default-max-pods-per-node "110" \
  --enable-autoscaling --min-nodes "1" --max-nodes "6" --no-enable-master-authorized-networks \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair \
  --enable-vertical-pod-autoscaling

# 2nd CPU pool
#gcloud container --project "${PROJECT}" node-pools create "pool-1" --cluster "${CLUSTER}" --zone "${ZONE}" \
#  --machine-type "e2-custom-32-65536" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true \
#  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
#  --num-nodes "1" --enable-autoupgrade --enable-autorepair --min-nodes 1 --max-nodes 6 --enable-autoscaling \
#  --max-surge-upgrade 1 --max-unavailable-upgrade 0

# GPU pool
#GPU_POOL="gpu-pool-1"
#gcloud container --project "${PROJECT}" node-pools create "${GPU_POOL}" --cluster "${CLUSTER}" --zone "${ZONE}" \
#  --machine-type "n1-highmem-32" --accelerator "type=nvidia-tesla-v100,count=4" \
#  --image-type "COS" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true \
#  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
#  --num-nodes "1" --enable-autoupgrade --enable-autorepair --min-nodes 1 --max-nodes 5 --enable-autoscaling
#kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/master/nvidia-driver-installer/cos/daemonset-preloaded.yaml

gcloud container clusters get-credentials "${CLUSTER}" --zone "${ZONE}" --project "${PROJECT}"
gcloud config list
