#!/usr/bin/env bash

# http://docs.embassy.ebi.ac.uk/userguide/User_Guide.html#s3

source /Users/davidyuan/IdeaProjects/davidyuan/vault/s3/caas-ccresops.prop

# shellcheck disable=SC2154
export AWS_ACCESS_KEY_ID=${accesskey}
# shellcheck disable=SC2154
export AWS_SECRET_ACCESS_KEY=${secretkey}
# shellcheck disable=SC2154
export S3_ENDPOINT=${s3_endpoint}
# shellcheck disable=SC2154
export BUCKET_NAME=${bucket_name}

aws --endpoint-url "https://${S3_ENDPOINT}" s3 rm --recursive "s3://${BUCKET_NAME}/results"
aws --endpoint-url "https://${S3_ENDPOINT}" s3 ls --recursive "s3://${BUCKET_NAME}/"
