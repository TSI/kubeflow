#!/usr/bin/env python3

import yaml
from kfp.compiler import Compiler
from kfp.dsl import pipeline, ContainerOp, VolumeOp, PipelineVolume, ParallelFor, Sidecar, ExitHandler
from kubernetes.client import V1EnvVar, V1SecurityContext, V1PersistentVolumeClaim
from kubernetes.client.models import V1Volume

# All regions
# REGIONS = ['GL000207.1', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17',
#            '18', '19', '20', '21', '22', 'MT', 'X', 'Y']

# Multiple regions
# REGIONS = ['GL000207.1', 'MT', 'Y']

# Two regions
REGIONS = ['GL000207.1', 'MT']

# Single rergion
# REGIONS = ['GL000207.1']

DS_HUMAN_G1K_V37 = '/datasource/human_g1k_v37'
OUTPUT_PATH = '/workspace/results'
PVC_1000G = 'pv1000g'
PVC_WORKSPACE = 'shared-workspace'


def k8s_vol_op(name: str,
               manifest: str,
               action: str = "apply") -> VolumeOp:
    with open(manifest, 'r') as fp:
        yml = yaml.safe_load(fp)
        print(yml)
        k8s_resource = V1PersistentVolumeClaim(api_version=yml['apiVersion'],
                                               kind=yml['kind'],
                                               metadata=yml['metadata'],
                                               spec=yml['spec'])
        rop = VolumeOp(
            name=name,
            k8s_resource=k8s_resource,
            action=action
        )
    return rop


# def vcf_op() -> VolumeOp:
#     vop = VolumeOp(
#         name='output_vcf',
#         resource_name="output_vcf",
#         size="10Gi",
#         # storage_class='nfs-client',
#         modes=dsl.VOLUME_MODE_RWO
#     )
#     return vop


def samtools_op(accesskey: str,
                secretkey: str,
                s3_endpoint: str,
                bucket_name: str,
                output_vol: V1Volume,
                ref_path: str = DS_HUMAN_G1K_V37,
                image: str = 'davidyuyuan/samtools:1000g') -> ContainerOp:
    queries = Sidecar(name='aws',
                      image='davidyuyuan/aws:1000g',
                      command=['sh', '-c'],
                      args=['cp-queries.sh $0', ref_path],
                      mirror_volume_mounts=True)
    queries.set_image_pull_policy('Always')
    queries.add_env_variable(V1EnvVar(name='AWS_ACCESS_KEY_ID', value=accesskey)) \
        .add_env_variable(V1EnvVar(name='AWS_SECRET_ACCESS_KEY', value=secretkey)) \
        .add_env_variable(V1EnvVar(name='S3_ENDPOINT', value=s3_endpoint)) \
        .add_env_variable(V1EnvVar(name='BUCKET_NAME', value=bucket_name))

    op = ContainerOp(name='Samtools',
                     image=image,
                     command=['sh', '-c'],
                     # arguments=['get-reference-genome.sh $0; sleep infinity & wait', ref_path],
                     arguments=['get-reference-genome.sh $0', ref_path],
                     pvolumes={ref_path: output_vol},
                     sidecars=[queries])
    op.container.set_image_pull_policy('Always')
    return op


def freebayes_op(batch_size: int,
                 region: str,
                 input_vol: V1Volume,
                 output_vol: V1Volume,
                 host: str,
                 token: str,
                 insecure: bool = False,
                 ref_path: str = DS_HUMAN_G1K_V37,
                 output_path: str = OUTPUT_PATH,
                 image: str = 'davidyuyuan/freebayes:1000g') -> ContainerOp:
    op = ContainerOp(
        name='Freebayes',
        image=image,
        command=['sh', '-c'],
        arguments=['oneclient /mnt/oneclient/; run-freebayes.sh $0 $1 $2 $3', region, ref_path, output_path, batch_size],
        # arguments=['oneclient /mnt/oneclient/; run-freebayes.sh $0 $1 $2 $3; sleep infinity & wait', region, ref_path, output_path, batch_size],
        pvolumes={ref_path: input_vol, output_path: output_vol}  # ,
    )
    op.container.set_image_pull_policy('Always')
    # op.container.set_memory_request("128Ki")
    # op.container.set_cpu_request("10m")
    # op.container.add_resource_request("ephemeral-storage", "512Mi")          # for 12
    op.container.set_security_context(V1SecurityContext(privileged=True))  # for oneclient
    op.container.add_env_variable(V1EnvVar(name='ONECLIENT_INSECURE', value=insecure)) \
        .add_env_variable(V1EnvVar(name='ONECLIENT_PROVIDER_HOST', value=host)) \
        .add_env_variable(V1EnvVar(name='ONECLIENT_ACCESS_TOKEN', value=token))
    return op


def upload_op(accesskey: str,
              secretkey: str,
              s3_endpoint: str,
              bucket_name: str,
              input_vol: V1Volume,
              output_vol: V1Volume,
              ref_path: str = DS_HUMAN_G1K_V37,
              output_path: str = OUTPUT_PATH) -> ContainerOp:
    op = ContainerOp(
        name='S3 results',
        image='davidyuyuan/aws:1000g',
        command=['sh', '-c'],
        arguments=['cp-results.sh $0', output_path],
        pvolumes={ref_path: input_vol, output_path: output_vol}  # ,
        # sidecars=[aws]
    )
    op.container.set_image_pull_policy('Always')
    # op.container.add_resource_request("ephemeral-storage", "64Mi")     # for 12
    op.container.add_env_variable(V1EnvVar(name='AWS_ACCESS_KEY_ID', value=accesskey)) \
        .add_env_variable(V1EnvVar(name='AWS_SECRET_ACCESS_KEY', value=secretkey)) \
        .add_env_variable(V1EnvVar(name='S3_ENDPOINT', value=s3_endpoint)) \
        .add_env_variable(V1EnvVar(name='BUCKET_NAME', value=bucket_name))
    return op


def exit_op(accesskey: str,
            secretkey: str,
            s3_endpoint: str,
            bucket_name: str,
            image: str = 'davidyuyuan/aws:1000g') -> ContainerOp:
    op = ContainerOp(
        name='List results',
        image=image,
        is_exit_handler=True,
        command=['sh', '-c'],
        # arguments=['ls-r.sh; sleep infinity & wait']
        arguments=['ls-r.sh']
    )
    op.container.set_image_pull_policy('Always')
    op.container.add_env_variable(V1EnvVar(name='AWS_ACCESS_KEY_ID', value=accesskey)) \
        .add_env_variable(V1EnvVar(name='AWS_SECRET_ACCESS_KEY', value=secretkey)) \
        .add_env_variable(V1EnvVar(name='S3_ENDPOINT', value=s3_endpoint)) \
        .add_env_variable(V1EnvVar(name='BUCKET_NAME', value=bucket_name))
    return op


@pipeline(name='1000g Variant',
          description='Processes 1000g alignment and creates VCFs')
def align_and_vc(accesskey: str,
                 secretkey: str,
                 oneclient_access_token: str,
                 oneclient_insecure: bool = True,
                 oneclient_provider_host: str = '1000genomeop.embl.tk',
                 s3_endpoint: str = 's3.embassy.ebi.ac.uk',
                 bucket_name: str = 'caas-resops',
                 batch_size: int = 1):
    exit_task: ContainerOp = exit_op(accesskey=accesskey,
                                     secretkey=secretkey,
                                     s3_endpoint=s3_endpoint,
                                     bucket_name=bucket_name)

    with ExitHandler(exit_op=exit_task):
        human_g1k_v37: VolumeOp = k8s_vol_op(name='human_g1k_v37', manifest='./yaml/pvc-1000g.yml')
        samtools_task: ContainerOp = samtools_op(accesskey=accesskey,
                                                 secretkey=secretkey,
                                                 s3_endpoint=s3_endpoint,
                                                 bucket_name=bucket_name,
                                                 output_vol=human_g1k_v37.volume)
        vcf_output: VolumeOp = k8s_vol_op(name='output_vcf', manifest='./yaml/pvc-workspace.yml')

        with ParallelFor(REGIONS) as region:
            # vcf_output: VolumeOp = vcf_op()
            freebayes_op(batch_size=batch_size,
                         region=region,
                         input_vol=PipelineVolume(pvc=PVC_1000G).after(samtools_task),
                         output_vol=PipelineVolume(pvc=PVC_WORKSPACE).after(vcf_output),
                         host=oneclient_provider_host,
                         token=oneclient_access_token,
                         insecure=oneclient_insecure)

        upload_op(accesskey=accesskey,
                  secretkey=secretkey,
                  s3_endpoint=s3_endpoint,
                  bucket_name=bucket_name,
                  input_vol=PipelineVolume(pvc=PVC_1000G).after(samtools_task),
                  output_vol=PipelineVolume(pvc=PVC_WORKSPACE).after(vcf_output))


if __name__ == '__main__':
    Compiler().compile(align_and_vc, __file__ + '.tar.gz')
