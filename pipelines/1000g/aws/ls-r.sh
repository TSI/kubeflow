#!/usr/bin/env bash

# http://docs.embassy.ebi.ac.uk/userguide/User_Guide.html#s3

#upload_path=$1

# In case there are files to be uploaded left in cache
#BUCKET_NAME='caas-resops'
#OBJ_PATH='/results/freebase'

#s3_url="s3://${BUCKET_NAME}${OBJ_PATH}"
#aws --endpoint-url "https://${S3_ENDPOINT}" s3 mb ${s3_url}

#aws --endpoint-url "https://${S3_ENDPOINT}" s3 mv --recursive  --exclude ".nfs*" "${upload_path}" ${s3_url}

# List what is uploaded to the bucket
echo aws --endpoint-url "https://${S3_ENDPOINT}" s3 ls --recursive "s3://${BUCKET_NAME}/"
aws --endpoint-url "https://${S3_ENDPOINT}" s3 ls --recursive "s3://${BUCKET_NAME}/"
