#!/usr/bin/env bash

result_path=$1
## Debug!!!
#upload_path='/Users/davidyuan/IdeaProjects/davidyuan/kubeflow/trash'

upload_path="${result_path}/upload"
output_path="${result_path}/tmp"

BUCKET_NAME='caas-resops'
OBJ_PATH='/results/freebase'

s3_url="s3://${BUCKET_NAME}${OBJ_PATH}"
aws --endpoint-url "https://${S3_ENDPOINT}" s3 mb ${s3_url}

# wait till both paths exist and there are results to be uploaded
while true; do
  if [ -d "${upload_path}" ] && [ -d "${output_path}" ] && [ "$( ls -A "${upload_path}" )" ]; then
    break
  fi
done

while true; do
  aws --endpoint-url "https://${S3_ENDPOINT}" s3 mv --recursive --exclude ".nfs*" "${upload_path}" ${s3_url}
  if [ ! "$( ls -A "${upload_path}" )" ] && [ ! "$( ls -A "${output_path}" )" ]; then
    break
  fi
done
