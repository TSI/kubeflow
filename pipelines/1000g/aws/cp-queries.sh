#!/usr/bin/env bash

# http://docs.embassy.ebi.ac.uk/userguide/User_Guide.html#s3

ref_path=$1

OBJ_PATH='/queries.in'
s3_url="s3://${BUCKET_NAME}${OBJ_PATH}"

aws --endpoint-url "https://${S3_ENDPOINT}" s3 cp "${s3_url}" "${ref_path}${OBJ_PATH}"