#!/bin/bash

ref_path=$1

samtools

mkdir -p "${ref_path}"
cd "${ref_path}" || exit

[ -f human_g1k_v37.fasta.gz ] || \
#    wget --tries='inf' ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/human_g1k_v37.fasta.gz
    curl -O ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/human_g1k_v37.fasta.gz
gzip -dv human_g1k_v37.fasta.gz

< "${ref_path}/human_g1k_v37.fasta" \
  awk '/^>/ {if(N>0) printf("\n"); printf("%s\n",$0);++N;next;} { printf("%s",$0);} END {printf("\n");}' | \
  split -l 2 - seq_

for f in seq_a*
do
  j=$(head -1 "$f" | tr -d '>' | awk '{ print $1 }')
  echo "$f" "$j"
  cp "$f" "human_g1k_v37.${j}.fasta"
done

for f in human_g1k_v37.*.fasta; do samtools faidx "$f"; done

# clean up and display results
rm -f seq_*
ls -l