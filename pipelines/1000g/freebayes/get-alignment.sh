#!/usr/bin/env bash

# HG00099/alignment/HG00099.mapped.ILLUMINA.bwa.GBR.low_coverage.20130415.bam
query=$1

to_file=/datasource/data/${query}
mkdir -p "$(dirname "${to_file}")"

[ -f "${to_file}" ] || \
    wget --tries='inf' -O "${to_file}" "ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/${query}"
[ -f "${to_file}.bai" ] || \
    wget --tries='inf' -O "${to_file}.bai" "ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/${query}.bai"

echo "${to_file}"