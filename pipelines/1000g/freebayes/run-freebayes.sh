#!/bin/bash

freebayes

region=$1
ref_path=$2
result_path=$3
batch_size=$4

base='/mnt/oneclient/1000g'
region_ref=${region}
upload_path="${result_path}/upload"
output_path="${result_path}/tmp"
counter=0

mkdir -p "${output_path}" "${upload_path}"

# Mark the starting of a run
touch "${output_path}/${region}"

while IFS= read -r query
do
  fname=$(echo "${query}" | cut -d '/' -f 3 | cut -d '.' -f -7)
  to_file=${base}/${query}

  date
  echo freebayes \
    --region "${region}" \
    --fasta-reference "${ref_path}/human_g1k_v37.${region_ref}.fasta" \
    --vcf "${output_path}/${fname}.${region}.vcf" \
    "${to_file}"

  # Run it on query on the same region at a time!
  freebayes \
    --region "${region}" \
    --fasta-reference "${ref_path}/human_g1k_v37.${region_ref}.fasta" \
    --vcf "${output_path}/${fname}.${region}.vcf" \
    "${to_file}" &

  ((counter++))
  if [ ${counter} -ge "${batch_size}" ]; then
    counter=0 && wait
    ls -l "${output_path}"/*."${region}".vcf
    mv "${output_path}"/*."${region}".vcf "${upload_path}"
    ls -l "${upload_path}"/*."${region}".vcf
  fi
done < "${ref_path}/queries.in"

# Signal the end of a run and move the rest of the vcf for upload
rm "${output_path}/${region}"
ls -l "${output_path}"/*."${region}".vcf
mv "${output_path}"/*."${region}".vcf "${upload_path}"
ls -l "${upload_path}"/*."${region}".vcf

