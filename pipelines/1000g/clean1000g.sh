#!/usr/bin/env bash

kubectl get po -n kubeflow | grep 1000g | cut -d ' ' -f 1 | xargs kubectl delete po -n kubeflow
kubectl delete pvc -n kubeflow pv1000g shared-workspace

