#!/usr/bin/env bash

# download and install Vagrant at https://www.vagrantup.com/downloads.html
# download and install Virtual Box at https://www.virtualbox.org/wiki/Downloads

# More at https://www.kubeflow.org/docs/started/workstation/getting-started-minikf/
mkdir -p "${HOME}/minikf"
cd "${HOME}/minikf" || exit
vagrant init arrikto/minikf
vagrant up
