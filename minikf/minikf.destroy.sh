#!/usr/bin/env bash

cd "${HOME}/minikf" || exit
vagrant halt
vagrant destroy
rm "minikf-user-data.vdi"
