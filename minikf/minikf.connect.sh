#!/usr/bin/env bash

cd "${HOME}/minikf" || exit
vagrant up
vagrant ssh
