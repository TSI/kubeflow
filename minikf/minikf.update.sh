#!/usr/bin/env bash

cd "${HOME}/minikf" || exit
vagrant halt
vagrant box update
vagrant plugin update vagrant-persistent-storage
vagrant destroy
vagrant up
vagrant box list
