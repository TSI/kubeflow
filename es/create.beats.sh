#!/usr/bin/env bash

# https://www.elastic.co/webinars/application-observability-on-kubernetes-with-the-elastic-stack-hands-on-tutorial?token=s8fjnveq85&ultron=kubernetes-webinar&blade=followup&hulk=email&mkt_tok=eyJpIjoiTm1ZellqUXpOemRqWlRjNCIsInQiOiJnSWJQOHd4N21UeWFKQUp2bldOeHNiN2RqcjREaFpXbWNOY3pqQTNRMGg3UXN3VFdacVoyQ3NUUG96TGtBZkw4eUZQYzdJbnlFRXBMXC9PNWVWWGtNaW05SGFPY2o1Y3lBS09OZTNKd005TmR6cnNOK3o5V1dwcExXWExGcGtxck0ifQ%3D%3D
cloud_id=$1
cloud_auth=$2
apm_url=$3
apm_token=$4

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl create secret generic cloud-secret --namespace=kube-system --from-literal=cloud-id="${cloud_id}" --from-literal=cloud-auth="${cloud_auth}"
kubectl create secret generic cloud-secret --from-literal=cloud-id="${cloud_id}" --from-literal=cloud-auth="${cloud_auth}"
kubectl create secret generic apm-secret --from-literal=apm-url="${apm_url}" --from-literal=apm-token="${apm_token}"
kubectl get secrets -A | grep '-secret'

#kubectl apply -f "${DIR}/filebeat-setup.yml"
#kubectl wait --for=condition=Complete --timeout=600s job/filebeat-init -n kube-system
#kubectl apply -f "${DIR}/filebeat.yml"

kubectl apply -f "${DIR}/metricbeat-setup.yml"
kubectl wait --for=condition=Complete --timeout=600s job/metricbeat-init -n kube-system
kubectl apply -f "${DIR}/metricbeat.yml"
