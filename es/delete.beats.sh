#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl delete -f "${DIR}/filebeat.yml"
kubectl delete -f "${DIR}/filebeat-setup.yml"

kubectl delete -f "${DIR}/metricbeat.yml"
kubectl delete -f "${DIR}/metricbeat-setup.yml"

kubectl create secret apm-secret
kubectl create secret cloud-secret
kubectl create secret cloud-secret --namespace=kube-system
