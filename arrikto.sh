#!/usr/bin/env bash

kfpassword=$1
kfapp=$2
kfdir=$3

## DIR where the current script resides
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Set the following kfctl configuration file:
#export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v0.7-branch/kfdef/kfctl_existing_arrikto.0.7.1.yaml"
#export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v1.1-branch/kfdef/kfctl_istio_dex.v1.1.0.yaml"
export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v1.2-branch/kfdef/kfctl_istio_dex.v1.2.0.yaml"

# Set KF_NAME to the name of your Kubeflow deployment. You also use this
# value as directory name when creating your configuration directory.
# For example, your deployment name can be 'my-kubeflow' or 'kf-test'.
export KF_NAME=${kfapp}

# Set the path to the base directory where you want to store one or more
# Kubeflow deployments. For example, /opt.
# Then set the Kubeflow application directory for this deployment.
export BASE_DIR=${kfdir}
export KF_DIR=${BASE_DIR}/${KF_NAME}

# Specify credentials for the default user.
export KUBEFLOW_USER_EMAIL="davidyuan@ebi.ac.uk"
export KUBEFLOW_PASSWORD=${kfpassword}

mkdir -p "${KF_DIR}"

cd "${KF_DIR}" && kfctl build -V -f "${CONFIG_URI}"

#export CONFIG_FILE=${KF_DIR}/kfctl_existing_arrikto.0.7.1.yaml
#export CONFIG_FILE=${KF_DIR}/kfctl_istio_dex.v1.1.0.yaml
export CONFIG_FILE=${KF_DIR}/kfctl_istio_dex.v1.2.0.yaml

# Customization can be made by editing ${CONFIG_FILE} before running kfctl apply.
kfctl apply -V -f "${CONFIG_FILE}"

kubectl -n kubeflow get all
kubectl patch service -n istio-system istio-ingressgateway -p '{"spec": {"type": "LoadBalancer"}}'

# Use non-default ID and password
# Edit the dex config with extra users.
# The password must be hashed with bcrypt with an at least 10 difficulty level.
# You can use an online tool like: https://passwordhashing.com/BCrypt
DEX_CONFIG=${KF_DIR}/dex-config.yaml
kubectl get configmap dex -n auth -o jsonpath='{.data.config\.yaml}' > "${DEX_CONFIG}"

sed -i'.bak' \
  -e 's/admin@kubeflow.org/'"${KUBEFLOW_USER_EMAIL}"'/' \
  -e 's/$2y$12$ruoM7FqXrpVgaol44eRZW.4HWS8SAvg6KYVVSCIwKQPBmTpCm.EeO/'"${KUBEFLOW_PASSWORD}"'/' \
  "${DEX_CONFIG}"
diff "${DEX_CONFIG}" "${DEX_CONFIG}.bak"

kubectl create configmap dex --from-file=config.yaml="${DEX_CONFIG}" -n auth --dry-run=client -oyaml | kubectl apply -f -
kubectl rollout restart deployment dex -n auth

# https://istio.io/docs/tasks/traffic-management/ingress/ingress-control/
#export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
#export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
#export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')

