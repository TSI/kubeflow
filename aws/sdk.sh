#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

aws_access_key_id=$1
aws_secret_access_key=$2
default_region_name=$3
default_output_format=$4

# Installation
pip3 install awscli --upgrade
aws --version

# Configuration
aws configure<<EOF
${aws_access_key_id}
${aws_secret_access_key}
${default_region_name}
${default_output_format}
EOF
aws eks list-clusters

# PATH=${PATH}:${CI_PROJECT_DIR}. The aws-iam-authenticator is under ${CI_PROJECT_DIR} and thus under ${PATH}.
curl -o "${DIR}/../aws-iam-authenticator" "https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator"
chmod +x "${DIR}/../aws-iam-authenticator"
aws-iam-authenticator version