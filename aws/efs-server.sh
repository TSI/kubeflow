#!/usr/bin/env bash

# Precondition: EFS is created manually in the same region and same VPC as the EKS cluster
# Add security group for ClusterSharedNodeSecurityGroup
# https://eu-west-2.console.aws.amazon.com/efs/home?region=eu-west-2#/filesystems
# https://docs.aws.amazon.com/efs/latest/ug/creating-using-create-fs.html
# https://helm.sh/docs/intro/install/
# https://github.com/helm/charts/tree/master/stable/efs-provisioner

awsRegion=$1
cluster=$2

token=efs-${awsRegion}-${cluster}

apt-get update && apt-get install -y jq

aws efs create-file-system \
  --creation-token "${token}" \
  --performance-mode generalPurpose \
  --throughput-mode bursting \
  --region "${awsRegion}" \
  --tags Key=Name,Value="${token}" Key=developer,Value=davidyuan

# FileSystemId
efsFileSystemId=$( aws efs describe-file-systems \
  --creation-token "${token}" \
  --query "FileSystems[?Name == '${token}'].FileSystemId" | jq -r .[] )

# securitygroup
securitygroup=$( aws cloudformation describe-stack-resources --stack-name "eksctl-${cluster}-cluster" \
  --query "StackResources[?LogicalResourceId=='ClusterSharedNodeSecurityGroup'].PhysicalResourceId" )

# subnets - [ comma-separated list ]
nodegroup=$( eksctl get nodegroup --cluster "${cluster}" --output json | jq -r '.[0].Name' )
nodegroupid=$( aws cloudformation describe-stack-resources --stack-name "eksctl-${cluster}-nodegroup-${nodegroup}" \
  --query "StackResources[?LogicalResourceId=='NodeGroup'].PhysicalResourceId" )
ORG_IFS=${IFS}; IFS=',' read -r -a subnets <<< "$( aws autoscaling describe-auto-scaling-groups \
  --auto-scaling-group-names "${nodegroupid}" \
  --query "AutoScalingGroups[].VPCZoneIdentifier" | jq -r .[] )"; IFS=${ORG_IFS}

# LifeCycleState available
while true
do
  state=$( aws efs describe-file-systems \
    --creation-token "${token}" \
    --query "FileSystems[].LifeCycleState" | jq -r .[] )
  echo "${token}": "${state}".
  if [ "${state}" == 'available' ]; then
      break
  fi
  sleep 10
done

for subnet in "${subnets[@]}"
do
  echo "${subnet}"
  echo "${securitygroup}"
  aws efs create-mount-target \
    --file-system-id "${efsFileSystemId}" \
    --subnet-id "${subnet}" \
    --security-groups "${securitygroup}"
done

for subnet in "${subnets[@]}"
do
  # LifeCycleState available
  while true
    do
      state=$( aws efs describe-mount-targets \
        --file-system-id "${efsFileSystemId}" \
        --query "MountTargets[?SubnetId == '${subnet}'].LifeCycleState" | jq -r .[] )
      echo "${subnet}": "${state}".
      if [ "${state}" == 'available' ]; then
        break
      fi
      sleep 10
    done
done

# Script above is not working. Use GUI to create FS and mount points instead.
# User the same VPC as the EKS cluster and security group for shared nodes
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm install 'efs' 'stable/efs-provisioner' \
  --set efsProvisioner.provisionerName='ebi.ac.uk/aws-efs' \
  --set efsProvisioner.storageClass.name='nfs-client' \
  --set efsProvisioner.efsFileSystemId="${efsFileSystemId}" \
  --set efsProvisioner.awsRegion="${awsRegion}"
kubectl rollout status deployment.apps/efs-efs-provisioner --timeout=6m
kubectl get storageclass
