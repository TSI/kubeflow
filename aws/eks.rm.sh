#!/usr/bin/env bash

name=$1
region=$2

eksctl delete cluster --region="${region}" --name="${name}" --wait
#aws eks wait cluster-deleted --name="${name}"