#!/usr/bin/env bash

# https://helm.sh/docs/intro/install/
# https://docs.aws.amazon.com/efs/latest/ug/creating-using-create-fs.html

awsRegion=$1
cluster=$2

apt-get update && apt-get install -y jq

# https://github.com/helm/charts/tree/master/stable/efs-provisioner
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm uninstall 'efs'
kubectl get storageclass

token=efs-${awsRegion}-${cluster}

# FileSystemId
efsFileSystemId=$( aws efs describe-file-systems \
  --creation-token "${token}" \
  --query "FileSystems[?Name == '${token}'].FileSystemId" | jq -r .[] )

# mounttargets
read -r -a mounttargets <<< "$( aws efs describe-mount-targets --file-system-id "${efsFileSystemId}" \
  --query "MountTargets[].MountTargetId" | jq -r .[] | xargs echo )"
for mounttarget in "${mounttargets[@]}"
do
  echo "${efsFileSystemId}": "${mounttarget}"
  aws efs delete-mount-target --mount-target-id "${mounttarget}"
done

# wait and delete efs
while true
do
  targets=$( aws efs describe-file-systems --file-system-id "${efsFileSystemId}" \
    --query "FileSystems[].NumberOfMountTargets" | jq -r .[] )
  echo "${efsFileSystemId}": "${targets}"
  if [ "${targets}" -eq 0 ]; then
      break
  fi
  sleep 10
done
aws efs delete-file-system --file-system-id "${efsFileSystemId}"