#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

name=$1           # name='tsi-aws-1'
region=$2         # region='eu-west-2'

keyname=${name}'.id_rsa'
pubkeyname=${keyname}'.pub'
gpu-pool="gpu1"

# Generating a new RSA key pair with public key in ./${name}.id_rsa.pub
ssh-keygen -t rsa -f "${DIR}/${keyname}" -P ''

# CPU only --node-type=m5.2xlarge
eksctl create cluster --name="${name}" --region="${region}" --nodes-max=8 --node-type=m5.2xlarge \
  --ssh-access  --ssh-public-key="${DIR}/${pubkeyname}" --tags="Owner=David Yuan,Team=TSI Team"

# CPU GPU --node-type=p3.2xlarge
#eksctl create nodegroup --cluster="${name}" --name="${gpu-pool}" --nodes-max=5 --node-type=p3.2xlarge \
#  --ssh-access  --ssh-public-key="${DIR}/${pubkeyname}"
#kubectl create -f https://github.com/NVIDIA/k8s-device-plugin/raw/master/nvidia-device-plugin.yml
