#!/usr/bin/env bash

kubectl apply -f https://github.com/kubernetes/dashboard/raw/master/aio/deploy/recommended.yaml
kubectl apply -f https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/raw/external/tsi-cc/ResOps/scripts/k8s-dashboard/admin-user.yml

kubectl -n kubernetes-dashboard describe secret "$(kubectl -n kubernetes-dashboard get secret | grep admin-user-token | awk '{print $1}')"
kubectl patch service -n kubernetes-dashboard kubernetes-dashboard -p '{"spec": {"type": "LoadBalancer"}}'
