#!/usr/bin/env python3

import bcrypt

passwd = b'secret'

salt = bcrypt.gensalt()
hashed = bcrypt.hashpw(passwd, salt)

print(hashed)
