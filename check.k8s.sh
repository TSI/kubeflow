#!/usr/bin/env bash

kubectl version
kubectl config get-contexts
kubectl cluster-info
kubectl get node
kubectl get pod --all-namespaces
kubectl get nodes "-o=custom-columns=NAME:.metadata.name,GPU:.status.allocatable.nvidia\.com/gpu"
kubectl get svc -A